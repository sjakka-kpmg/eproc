# Nexus Entities MDM Sync PAPI
This is a Mule 4.2.x Process API

----

### History

|	Date	    |	Author	        |	Remarks				|
|	---------   |	--------------	|	------------------	|
|	2020-11-10	|	Max Girin	|	Initial Project		|


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Maven
JDK 1.8.x or later
Studio 7.3.5 or later
Studio Mule Runtime 4.1.5 or later
```

### Installing

A step by step series of examples that tell you have to get a development env running

###### Step 1: Check out the code from KPMG BitBucket Git (HTTPS/SSH):

**HTTPS**

```
git clone https://git.us.kworld.kpmg.com/projects/SFDCCRM/repos/mule-nexus-entities-mdm-sync-papi -b master
```


###### Step 2: Import the project to MuleSoft Anypoint Studio

- Download or clone the project

```
https://git.us.kworld.kpmg.com/projects/SFDCCRM/repos/mule-nexus-entities-mdm-sync-papi
```

-  Click on file-> import
-  Choose  Anypoint studio project from File System
-  Select the file location and Uncheck copy the project into workspace option.

##Listener Port: You can override this by defining the value at Runtime Properties or adding it in the Runtime Argument (ie: -Dapi.http.port=8091)

 - api.http.port:8091
 - api.https.port:8092

 ##HTTP(s) URL Base:

   - **For Listener with HTTP (Non-secured) protocol:**
     - http://localhost:8091

   - **For Listener with HTTPS (Secured) protocol:**
     - https://localhost:8092

## Running the tests
To Run the Munits and check the code coverage
-   In the Package explorer, Traverse to src/test/munit
-   Right click on api-apikit-test.xml
-   click on Munit -> Run tests
-   The console output as well as Mule errors(if any), code coverage by the Munits can be seen in separate tabs in the left bottom of the Anypoint studio.
-   click on generate report button in the code.

## Deployment

Add additional notes about how to deploy this on a live system
1. Create a Mule deployable Archive from the project in Anypoint Studio.
2. Update the application file with the latest archive file generated in the previous step.
3. Update runtime version (if applicable).
4. Update the Properties with relevant information and restart the application.
-   Give the headers(client_id, client_secret)

## Built With

* [Mulesoft](http://https://www.mulesoft.com/platform/enterprise-integration) - Anypoint Platform
* [Maven](https://maven.apache.org/) - Dependency Management
* [RAML](http://raml.org) - API Designer

## Versioning

We use [BitBucket](http://bitbucket.org/) for versioning.

### FAQ

+##### How to add Maven Exchange Repository?
-   Open your Maven Settings (<M2_HOME>/settings.xml)
- 	Add the following config under `<servers>` tag for your Exchange Login Credential
  ```
<servers>
....  
		<server>
		  <id>KPMGExchange</id>
		  <username><REPLACE_WITH_ANYPOINT_USERNAME></username>
		  <password><REPLACE_WITH_ANYPOINT_PASSWORD></password>
	</server>
...
</servers>
  ```

##### How to run multiple applications at the same time (experience, process, system) ?
-   Make sure the port numbers are different for all the apis
-   Appropriate client ID and secrets are given in the global properties

##### How to solve 503 error ?
-   503 error is caused by a new feature introduced in 3.8x and later, the Gatekeeper.
-   Gatekeeper is a process on the mule server which by default blocks any incoming request to the API if the API has not been successfully paired with API Manager to receive appropriate policies. This security setting means that by default - if a policy is not applied, the API cannot be accessed.
-    To avoid the error.
       -   while deploying application, Right click on project->Run As -> Run Configurations (This can also be done by clicking on the run tab in the anypoint studio Menu -> Run configurations)
       -   Go to Arguments Tab, In VM Arguments add the following command
       ```
       -Danypoint.platform.gatekeeper=disabled
       ```
-   Click Apply and run the application.

##### How to solve 'MuleEncryptionException' error ?
-   MuleEncryptionException error is caused because the encryption key provided when running the application is different or one is not provided when deploying the application.
-   This secure setting is to provide a encryption feature to the secure data saved in the project when implemented.
-    To avoid the error.
       -   while deploying application, Right click on project->Run As -> Run Configurations (This can also be done by clicking on the run tab in the Anypoint Studio Menu -> Run configurations)
       -   Go to Arguments Tab, In VM Arguments add the following command
       ```
       -Dxh.runtime.key=XXXXXXXXXXXXXXXX (length of the key should be minimum of 16 characters)
       ```
-   Click Apply and run the application

#### How are squashed pull request matched?

## Authors

* **Max Girin** - *MG* - [mgirin](https://git.us.kworld.kpmg.com/mgirin)
