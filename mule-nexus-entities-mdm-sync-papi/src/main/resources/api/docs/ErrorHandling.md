# **Error Handling**

In case of any failure, logs will be printed in Anypoint Platform Cloudhub logs in a JSON format.

This API will send the below HTTP status code along with response in case of error:
- HTTP: 400 :- Below is the sample response in case of HTTP 400 error
    ```
    {"code":"400","message":"BAD_REQUEST","description":"Bad Request","transactionId":"803ec5202bd54576992d082da70e6338"}
    ```
- HTTP: 403 :- Below is the sample response in case of HTTP 403 error
    ```  
    {"code":"403","message":"ACCESS_DENIED","description":"Problem With The Credentials That Were Provided","transactionId":"803ec5202bd54576992d082da70e6338"}
    ```
- HTTP: 404 :- Below is the sample response in case of HTTP 404 error
    ```      
    {"code":"404","message":"RESOURCE_NOT_FOUND","description":"The Resource Cannot Be Found","transactionId":"803ec5202bd54576992d082da70e6338"}
    ```
- HTTP: 405 :- Below is the sample response in case of HTTP 405 error
    ```      
    {"code":"405","message":"METHOD_NOT_ALLOWED","description":"Method Not Allowed","transactionId":"803ec5202bd54576992d082da70e6338"}
    ```
- HTTP: 500 :- Below is the sample response in case of HTTP 500 error
    ```  
    {"code":"500","message":"INTERNAL_SERVER_ERROR","description":"Internal Server Error","transactionId":"803ec5202bd54576992d082da70e6338"}
    ```
- HTTP: 599 :- Below is the sample response in case of HTTP 599 error
    ```  
    {"code":"599","message":"SYS_TIMEOUT","description":"System API Dependent Service Timeout","transactionId":"803ec5202bd54576992d082da70e6338"}