### Version 0.0.0
  - Initial Template 
    - Updated by: Max Girin- 2020-10-30
### Version 0.0.1
  - Update with RAML
  - Updated by: Max Girin - 2020-10-30
### Version 0.0.2
  - Added Informatica MDM and SAP Sync XSD schema types
  - Updated by: Max Girin - 2020-11-02

### Published History
---
- Asset Version: **0.0.1**
- Published By: **Max Girin**
- Date / Time: **2020-10-30 at 2235**
---
- Asset Version: **0.0.2**
- Published By: **Max Girin**
- Date / Time: **2020-11-02 at 1735**
---