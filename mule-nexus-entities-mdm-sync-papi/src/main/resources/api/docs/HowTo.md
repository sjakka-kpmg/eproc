# **Nexus Entities SAP Sync System API**
---
### **How To Use The API?**
- HTTPs POST  methods will be used for **Nexus Entities SAP Sync SAPI**.
###### *Details of the Methods:*
 - **/api-status:/GET** Get API status.
 - **/api-status:/POST** Get API status.
 - **/parties:/POST** Create an entity in SAP Sync.
 - **/parties/party-search:/POST** Get an entity out of SAP Sync by global party id.
 - **/parties/{globalPartyId}/events/inbound:/POST** Generate a new inbound Entity event into SAP Sync.
 - **/parties/{globalPartyId}/events/outbound:/POST** Generate a new outbound Entity event out of SAP Sync.
 - **/parties/{globalPartyId}:/GET** Get an entity out of SAP Sync by global party id.
 - **/parties/{globalPartyId}:/DELETE** Delete an existing entity based on global party id.
 - **/parties/{globalPartyId}:/PATCH** Update an existing entity based on global party id with specific attributes only.
 - **/parties/{globalPartyId}:/PUT** Update an existing entity based on global party id.
