# **Nexus Entities SAP Sync System API**

## **Overview**:
  This System API deals with the Entities data to/from SAP Sync platform. Its responsible for various operations on Entities in SAP Sync.

## **Prerequisites**
- For version v1.0 :
  - Use **HTTP Basic authentication** to verify authorization.
  - Communicate using **HTTPS**
  - TLS v1.1 or latest
  - Must use **application/json** for all the payload content-type

## **Header Requirements**
- The following headers must be defined on each request (except for api status heartbeat operations):
  - `source`
  - `client_id`
  - `client_secret`
  - `x-transaction-id`
  - `Content-Type`

## **Security & Authentication**
- Client ID Enforcement Policy using HTTP Basic Authentication with username and password.