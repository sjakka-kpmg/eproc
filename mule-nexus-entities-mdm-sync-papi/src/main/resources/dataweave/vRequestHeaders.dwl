%dw 2.0
output application/java
---
{
	"clientId": attributes.headers["client_id"],
	"clientSecret": attributes.headers["client_secret"],
	"transactionId": attributes.headers["x-transaction-id"] default (correlationId default uuid()),
	"locale": attributes.headers["locale"],
	"siteid":  attributes.headers["siteid"],
	"source": attributes.headers["source"]
}